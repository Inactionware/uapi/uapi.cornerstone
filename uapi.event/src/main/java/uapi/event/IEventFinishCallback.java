package uapi.event;

/**
 * Created by xquan on 5/18/2017.
 */
public interface IEventFinishCallback {

    void callback();
}
